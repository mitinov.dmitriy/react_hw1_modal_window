import {SAVE_ITEMS, TOGGLE_ADDED_TO_FAVORITES, TOGGLE_REMOVED_FROM_CART, SET_IS_LOADING_FROM_SERVER}
    from "../actionTypes/actionTypes.item";

export const saveItems = (payload) => ({
    type: SAVE_ITEMS,
    payload,
})

export const setIsItemAddedToFavorite = (payload) => ({
    type: TOGGLE_ADDED_TO_FAVORITES,
    payload,
})

export const setIsItemRemovedFromCart = (payload) => ({
    type: TOGGLE_REMOVED_FROM_CART,
    payload,
})

export const setIsLoadingFromServer = (payload) => ({
    type: SET_IS_LOADING_FROM_SERVER,
    payload
})