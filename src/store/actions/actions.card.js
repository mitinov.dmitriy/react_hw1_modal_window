import {SET_CARDS_IN_CART, SET_CARDS_IN_FAVORITE} from "../actionTypes/actionTypes.card"

export const setCardsInCart = (payload) => ({
    type: SET_CARDS_IN_CART,
    payload
})

export const setCardsInFavorite = (payload) => ({
    type: SET_CARDS_IN_FAVORITE,
    payload
})