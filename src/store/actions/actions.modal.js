import {SET_MODAL_TO_SHOW} from "../actionTypes/actionTypes.modal";

export const setModalToShow = (payload) => ({
    type: SET_MODAL_TO_SHOW,
    payload,
})