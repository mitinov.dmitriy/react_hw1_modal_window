import {TOGGLE_OPEN_FORM} from "../actionTypes/actionTypes.ckeckOutForm"

export const setToggleOpenCheckoutForm = (payload) => ({
    type: TOGGLE_OPEN_FORM,
    payload
})