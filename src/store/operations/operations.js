import {saveItems, setIsLoadingFromServer} from "../actions/actions.item";
import axios from "axios";

export const getItems = () => (dispatch) => {
    axios("/goodsListing/goodsListing.json")
        .then(({data}) => {
            dispatch(setIsLoadingFromServer(false));
            dispatch(saveItems(data.goods));
        })
}