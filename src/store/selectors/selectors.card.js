export const cardsInCart = state => state.card.cardsInCart;
export const cardsInFavorite = state => state.card.cardsInFavorite;