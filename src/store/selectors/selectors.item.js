export const itemsSelector = state => state.items.items;
export const isItemAddedToFavoriteSelector = state => state.items.toggleAddedToFavorite;
export const isItemRemovedFromCartSelector = state => state.items.toggleRemovedFromCart;
export const isLoadingFromServer = state => state.items.isLoadingFromServer;