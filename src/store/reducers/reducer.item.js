import {SAVE_ITEMS, TOGGLE_ADDED_TO_FAVORITES, TOGGLE_REMOVED_FROM_CART, SET_IS_LOADING_FROM_SERVER} from "../actionTypes/actionTypes.item";
import { createReducer } from '@reduxjs/toolkit'

const initialState = {
    items: [],
    toggleAddedToFavorite: false,
    toggleRemovedFromCart: false,
    isLoadingFromServer: true,
}

const itemsReducer = createReducer(initialState, (builder) => {
    builder
        .addCase(SAVE_ITEMS, (state, action) => {
            state.items = action.payload;
        })
        .addCase(TOGGLE_ADDED_TO_FAVORITES, (state, action) => {
            state.toggleAddedToFavorite = action.payload;
        })
        .addCase(TOGGLE_REMOVED_FROM_CART, (state, action) => {
            state.toggleRemovedFromCart = action.payload;
        })
        .addCase(SET_IS_LOADING_FROM_SERVER, (state, action) => {
            state.isLoadingFromServer = action.payload;
        })
})

export default itemsReducer;