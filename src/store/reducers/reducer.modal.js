import {SET_MODAL_TO_SHOW} from "../actionTypes/actionTypes.modal";
import { createReducer } from '@reduxjs/toolkit'

const initialState = {
    modalToShow: null,
}

const modalReducer = createReducer(initialState, (builder) => {
    builder
        .addCase(SET_MODAL_TO_SHOW, (state, action) => {
        state.modalToShow = action.payload;
    })
    }
)

export default modalReducer;