import {SET_CARDS_IN_CART, SET_CARDS_IN_FAVORITE} from "../actionTypes/actionTypes.card"
import { createReducer } from '@reduxjs/toolkit'


const initialState = {
    cardsInCart: [],
    cardsInFavorite: [],
}

const cardReducer = createReducer(initialState, (builder) => {
    builder
        .addCase(SET_CARDS_IN_CART, (state, action) => {
            state.cardsInCart = action.payload;
        })
        .addCase(SET_CARDS_IN_FAVORITE, (state, action) => {
            state.cardsInFavorite = action.payload;
        })
})

export default cardReducer;