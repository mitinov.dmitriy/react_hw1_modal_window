import {TOGGLE_OPEN_FORM} from "../actionTypes/actionTypes.ckeckOutForm"
import { createReducer } from '@reduxjs/toolkit'

const initialState = {
    toggleShowCheckoutForm: false,
}

const checkoutReducer = createReducer(initialState, (builder) => {
    builder
        .addCase(TOGGLE_OPEN_FORM, (state, action) => {
            state.toggleShowCheckoutForm = action.payload;
        })
})

export default checkoutReducer;