import modalReducer from "./reducers/reducer.modal";
import thunk from "redux-thunk";
import { configureStore } from '@reduxjs/toolkit';
import itemsReducer from "./reducers/reducer.item";
import cardReducer from "./reducers/reducer.card";
import checkoutReducer from "./reducers/reducer.checkoutForm";

const reducers = {
   modal: modalReducer,
   items: itemsReducer,
   card: cardReducer,
   checkoutForm: checkoutReducer,
}

const store = configureStore({
   devTools: true,
   reducer: reducers,
   middleware: [thunk],
})


export default store;