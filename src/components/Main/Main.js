import React from 'react';
import {Link, Outlet} from "react-router-dom";
import "./Main.scss"

function Main(props) {
    return (
        <>
            <header className="page-header">
                <nav className="navigation_menu">
                    <ul className="navigation_menu_links">
                        <li className="navigation_menu_item"><Link className="navigation_menu_link"
                                                                   to="/products">Products</Link></li>
                        <li className="navigation_menu_item"><Link className="navigation_menu_link"
                                                                   to="/favorites">Favorites</Link></li>
                        <li className="navigation_menu_item"><Link className="navigation_menu_link"
                                                                   to="/cart">Cart</Link></li>
                    </ul>
                </nav>
            </header>
            <Outlet/>
        </>
    );
}

export default Main;