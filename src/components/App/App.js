import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import "./App.scss"
import ProductList from "../ProductsList/ProductList";
import Cart from "../Cart/Cart";
import Favorites from "../Favorites/Favorites";
import Main from "../Main/Main";
import useCardId from "../../hooks/useCardId";
import modalWindowParameters from "../../modalWindowData/modalWindowParameters";
import {useSelector, useDispatch} from "react-redux";
import {itemsSelector, isItemAddedToFavoriteSelector, isItemRemovedFromCartSelector} from "../../store/selectors/selectors.item"
import {modalToShowSelector} from "../../store/selectors/selectors.modal"
import {getItems} from "../../store/operations/operations";
import {setModalToShow} from "../../store/actions/actions.modal";
import {setIsItemAddedToFavorite, setIsItemRemovedFromCart} from "../../store/actions/actions.item";

function App() {
    const items = useSelector(itemsSelector);
    const isItemAddedToFavorite = useSelector(isItemAddedToFavoriteSelector);
    const isItemRemovedFromCart = useSelector(isItemRemovedFromCartSelector);
    const modalToShow = useSelector(modalToShowSelector);
    const [cartItemId, setCartItemId] = useCardId();
    const dispatch = useDispatch();

    const handleCardsRender = () => {
        if (!items.length) {
            dispatch(getItems())
        }
    }

    const toggleModal = (modalData) => {
        dispatch(setModalToShow(modalData));
    }

    const confirmAddToCart = () => {
        const currentCardId = items.find(item => item.id === cartItemId).id
        if (!localStorage.getItem("cartItems")) {
            const elementsInCart = [];
            elementsInCart.push(currentCardId)
            localStorage.setItem("cartItems", JSON.stringify(elementsInCart));
        } else {
            let inCartItems = JSON.parse(localStorage.getItem("cartItems"));
            if (inCartItems.includes(currentCardId)) {
                localStorage.setItem("cartItems", JSON.stringify(inCartItems));
            } else {
                inCartItems.push(currentCardId);
                localStorage.setItem("cartItems", JSON.stringify(inCartItems));
            }
        }
        dispatch(setModalToShow(null));
    }

    const removeFromCart = (e) => {
        const selectedModal = modalWindowParameters.find(({id}) => {
            return id === Number(e.target.dataset.modal);
        })
        dispatch(setModalToShow(selectedModal));
        setCartItemId(+e.target.dataset.cardid)
    }

    const confirmRemoveFromCart = () => {
        let currentItems = JSON.parse(localStorage.getItem("cartItems"));
        const currentCardId = items.find(item => item.id === cartItemId).id
        if (currentItems.includes(currentCardId)) {
            currentItems = currentItems.filter(item => item !== currentCardId);
            localStorage.setItem("cartItems", JSON.stringify(currentItems));
        }
        dispatch(setIsItemRemovedFromCart(!isItemRemovedFromCart));
        dispatch(setModalToShow(null));
    }


    const addToFavHandler = (value) => {
        dispatch(setIsItemAddedToFavorite(value))
    }

    const addToCartHandler = (id) => {
        setCartItemId(id)
    }

    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Main/>}>
                        <Route path="products" element={<ProductList items={items} onSubmitAddToCard={confirmAddToCart}
                         modalToShow={modalToShow} onRenderHandler={handleCardsRender} addToCartHandler={addToCartHandler}
                         addToFavHandler={addToFavHandler} toggleModal={toggleModal}/>}/>
                        <Route path="cart" element={<Cart  items={items} showRemoveFromCartModal={removeFromCart}
                                                           modalToShow={modalToShow} onRenderHandler={handleCardsRender}
                                                           onConfirmRemoveFromCart={confirmRemoveFromCart} addToFavHandler={addToFavHandler}
                                                           toggleModal={toggleModal} isItemRemovedFromCart={isItemRemovedFromCart}/>}/>
                        <Route path="favorites" element={<Favorites items={items} onSubmitAddToCard={confirmAddToCart}
                                                        modalToShow={modalToShow} onRenderHandler={handleCardsRender}
                                                        addToCartHandler={addToCartHandler} addToFavHandler={addToFavHandler}
                                                        toggleModal={toggleModal} isItemAddedToFavorite={isItemAddedToFavorite}/>}/>
                    </Route>
                </Routes>
            </BrowserRouter>
        </>
    );
}

export default App;