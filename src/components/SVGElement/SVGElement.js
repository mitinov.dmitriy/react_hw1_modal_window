import React from 'react';
import PropTypes from 'prop-types';
import "./SVGElement.scss"

function SvgElement(props) {
    return (
        <div className={props.className} onClick={props.handleClick}>
            {props.svg}
        </div>
    );
}

SvgElement.propTypes = {
    className: PropTypes.string,
    svg: PropTypes.element,
    handleClick: PropTypes.func
};


export default SvgElement;