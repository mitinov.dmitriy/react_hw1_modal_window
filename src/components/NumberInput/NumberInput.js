import React from 'react';
import NumberFormat from "react-number-format"
import {ErrorMessage, useField} from "formik";

function NumberInput(props) {
    const {name, cssClass, ...rest} = props;
    const [field] = useField(name)

    return (
        <>
            <NumberFormat {...field} format="(###) ###-##-##" allowEmptyFormatting mask="_" className={cssClass} {...rest}/>
            <ErrorMessage name={name} render={(message) => <span className="validation_error">
                    {message}
                </span>} />
        </>

    );
}

export default NumberInput;