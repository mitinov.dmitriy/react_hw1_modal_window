import React from 'react';
import "./Button.scss"
import PropTypes from "prop-types";

function Button(props) {
    return (
        <button data-cardid={props.cardId} data-modal={props.modalId} className={props.className}
                onClick={props.onAddToCart}>{props.text}</button>
    );
}

Button.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    text: PropTypes.string,
    modalId: PropTypes.string
};

Button.defaultProps = {
    text: "Click Me",
    modalId: "",
    cardId: ""
}

export default Button;