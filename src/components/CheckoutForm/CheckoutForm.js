import React from 'react';
import {withFormik, Field, Form, ErrorMessage} from "formik";
import "./CheckoutForm.scss";
import {connect} from "react-redux";
import {isCheckoutShown} from "../../store/selectors/selectors.checkOutForm";
import {setToggleOpenCheckoutForm} from "../../store/actions/actions.checkOutForm"
import * as Yup from "yup"
import NumberInput from "../NumberInput/NumberInput";
import {setCardsInCart} from "../../store/actions/actions.card"

const validationSchema = Yup.object().shape({
    firstName: Yup.string().required("this field is required"),
    lastName: Yup.string().required("this field is required"),
    age: Yup.string().max(3, "not valid age format").matches(/^\d+$/, 'Only numbers are allowed').required("this field is required"),
    shippingAddress: Yup.string().required("this field is required"),
    mobileNumber: Yup.string().required("this field is required")
})

const CheckoutForm = (props) => {
    const {cardsToCheckout, isFormShown, dispatch} = props

    const closeForm = () => {
        dispatch(setToggleOpenCheckoutForm(!isFormShown))
    }

    return(
    <div className="form-wrapper">
        <div className="form-container">
            <Form className="page-form">
                <span className="close" onClick={closeForm}/>
                <Field component="input"
                    type="text"
                    name="firstName"
                    className="page-form-field"
                    placeholder="First Name"
                />
                <ErrorMessage name="firstName" render={(message) => <span className="validation_error">
                    {message}
                </span>} />
                <Field component="input"
                    type="text"
                    name="lastName"
                    className="page-form-field"
                    placeholder="Last Name"
                />
                <ErrorMessage name="lastName" render={(message) => <span className="validation_error">
                    {message}
                </span>} />
                <Field component="input"
                    type="text"
                    name="age"
                    className="page-form-field"
                    placeholder="Age"
                />
                <ErrorMessage name="age" render={(message) => <span className="validation_error">
                    {message}
                </span>} />
                <Field component="input"
                    type="text"
                    name="shippingAddress"
                    className="page-form-field"
                    placeholder="Shipping Address"
                />
                <ErrorMessage name="shippingAddress" render={(message) => <span className="validation_error">
                    {message}
                </span>} />
                <NumberInput name="mobileNumber" value={props.mobileNumber} cssClass="page-form-field"/>
                <Field component="input"
                    name="submitButton"
                    type="submit"
                    value="Order"
                    className="submit_button"
                />
            </Form>
        </div>
    </div>

)};

const mapStateToProps = (state) => ({
    isFormShown: isCheckoutShown(state),
})

const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
})

const CheckoutFormFormik =  withFormik({
    mapPropsToValues: () => ({
        firstName: "",
        lastName: "",
        age: "",
        shippingAddress: "",
        mobileNumber: "",
    }),
    validationSchema: validationSchema,
    handleSubmit: (values, {props, ...actions}) => {
        setTimeout(() => {
            console.log("formData", values);
            console.log("ordered goods", props.cardsToCheckout);
            actions.resetForm()
            props.dispatch(setCardsInCart([]));
            props.dispatch(setToggleOpenCheckoutForm(!props.isFormShown));
            localStorage.removeItem("cartItems");
        }, 1500)
    }
})(CheckoutForm);

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutFormFormik)