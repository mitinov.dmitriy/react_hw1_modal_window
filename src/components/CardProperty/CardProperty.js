import "./CardProperty.scss"
import React from 'react';
import PropTypes from 'prop-types';

function CardProperty(props) {
    return (
        <span className={props.className}>
                {props.itemProperty} {props.propertyDescription}
            </span>
    );
}

CardProperty.propTypes = {
    className: PropTypes.string.isRequired,
    itemProperty: PropTypes.string.isRequired,
    propertyDescription: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};

CardProperty.defaultProps = {
    propertyDescription: "N/A"
}

export default CardProperty;