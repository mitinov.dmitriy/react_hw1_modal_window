import React from 'react';
import PropTypes from 'prop-types';
import "./Modal.scss"

function Modal(props) {
    const {onCloseModal, submitHandler} = props;

    const closeModal = () => {
        onCloseModal(null)
    }

    return (
        <div className="modal_wrapper" onClick={closeModal}>
            <div className="modal">
                <header className="modal_header">
                    <h2 className="modal_header_text">{props.header}</h2>
                    <span className="close" onClick={closeModal}/>
                </header>
                <div className="modal_body">
                    <p className="modal_body_text">
                        {props.text}
                    </p>
                    <div className="modal_body_buttons">
                        <button data-description="Ok button" onClick={submitHandler} className="modal_body_button">OK
                        </button>
                        <button data-description="decline button" className="modal_body_button"
                                onClick={closeModal}>Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}


Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    closeHandler: PropTypes.func,
    submitHandler: PropTypes.func,
    modalWrapperClickHandler: PropTypes.func
}

export default Modal;