import React from 'react';
import PropTypes from 'prop-types';
import "./CardImage.scss"

function CardImage(props) {
    return (
        <img src={props.imgUrl} alt={props.altText} className={props.className}/>
    );
}

CardImage.propTypes = {
    imgUrl: PropTypes.string,
    altText: PropTypes.string,
    className: PropTypes.string.isRequired
};

CardImage.defaultProps = {
    altText: "alt text by default",
    imgUrl: "https://via.placeholder.com/500"
}

export default CardImage;