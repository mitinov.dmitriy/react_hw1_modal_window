import React, {useEffect} from 'react';
import "./ProductsList.scss"
import Card from "../Card/Card";
import Modal from "../Modal/Modal";
import {useSelector} from "react-redux";
import {isLoadingFromServer} from "../../store/selectors/selectors.item"
import Loader from "../Loader/Loader";

function ProductList(props) {
    const {
        items,
        onSubmitAddToCard,
        modalToShow,
        onRenderHandler,
        addToCartHandler,
        addToFavHandler,
        toggleModal
    } = props;

    const isDataLoading = useSelector(isLoadingFromServer);

    useEffect(onRenderHandler);

    return (
        <div className="productsList">
            {isDataLoading && <Loader/>}
            {items && items.map(item => {
                return <Card isButtonShown={true} onAddToCart={addToCartHandler} id={item.id}
                             onAddToFavorites={addToFavHandler} toggleModal={toggleModal} svgFill={localStorage.getItem("favoriteItems")
                   && JSON.parse(localStorage.getItem("favoriteItems")).includes(item.id) ? "#F6BE00" : "darkgray"}
                             key={item.set_number} itemTitle={item.Title}
                             price={item.Price} color={item.Color}
                             setNumber={item.set_number} imgUrl={item.imageUrl} addToFav={addToFavHandler}
                             addToCart={toggleModal}/>
            })
            }
            {modalToShow &&
                <Modal onCloseModal={toggleModal}
                       header={modalToShow.modalHeaderText}
                       text={modalToShow.modalBodyText} submitHandler={onSubmitAddToCard}/>}
        </div>
    );
}

export default ProductList;