import React, {useEffect} from 'react';
import Card from "../Card/Card";
import "./Favorites.scss"
import Modal from "../Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {setCardsInFavorite} from "../../store/actions/actions.card"
import {isLoadingFromServer} from "../../store/selectors/selectors.item"
import Loader from "../Loader/Loader";
import {cardsInFavorite} from "../../store/selectors/selectors.card";

function Favorites(props) {
    let {
        items,
        addToCartHandler,
        isItemAddedToFavorite,
        addToFavHandler,
        onRenderHandler,
        onSubmitAddToCard,
        toggleModal,
        modalToShow
    } = props;

    const cards = useSelector(cardsInFavorite);
    const dispatch = useDispatch();

    const isDataLoading = useSelector(isLoadingFromServer);

    useEffect(onRenderHandler)

    useEffect(() => {
        const filteredCards = cards?.filter((card) => {
            return JSON.parse(localStorage.getItem("favoriteItems")).includes(card.id)
        })
        if (filteredCards) {
            dispatch(setCardsInFavorite(filteredCards));
        }
    }, [isItemAddedToFavorite])

    useEffect(() => {
        if (localStorage.getItem("favoriteItems")) {
            const favoriteCards = items?.filter((element) => {
                return JSON.parse(localStorage.getItem("favoriteItems")).includes(element.id);
            })
            if (favoriteCards) {
                dispatch(setCardsInFavorite(favoriteCards));
            }
        }
    }, [items])

    return (
        <div className="favorite_items">
            {isDataLoading && <Loader/>}
            {cards && cards.map(item => {
                return <Card isButtonShown={true} onAddToCart={addToCartHandler} id={item.id}
                             onAddToFavorites={addToFavHandler} toggleModal={toggleModal} svgFill="#F6BE00"
                             key={item.set_number} itemTitle={item.Title}
                             price={item.Price} color={item.Color}
                             setNumber={item.set_number} imgUrl={item.imageUrl} addToFav={addToFavHandler}
                             addToCart={toggleModal}/>
            })}
            {modalToShow && <Modal onCloseModal={toggleModal}
                                   header={modalToShow.modalHeaderText}
                                   text={modalToShow.modalBodyText} submitHandler={onSubmitAddToCard}/>}
        </div>
    );
}

export default Favorites;