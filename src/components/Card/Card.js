import "./Card.scss"
import React, {useEffect, useRef, useState} from 'react';
import modalWindowParameters from "../../modalWindowData/modalWindowParameters";
import Button from "../Button/Button";
import CardProperty from "../CardProperty/CardProperty";
import CardImage from "../CardImage/CardImage";
import SvgElement from "../SVGElement/SVGElement";


function Card(props) {
    const {onAddToFavorites, toggleModal, onAddToCart, svgFill, isButtonShown} = props;
    const [cartItemId, setCartItemId] = useState(null);
    const currentCard = useRef(null);

    useEffect(() => {
        setCartItemId(+currentCard.current.dataset.id);
    }, []);


    const addToCart = (e) => {
        const selectedModal = modalWindowParameters.find(({id}) => {
            return id === Number(e.target.dataset.modal);
        })
        onAddToCart(+e.target.dataset.cardid)
        toggleModal(selectedModal);
    }

    const addCardToFavorites = () => {
        if (!localStorage.getItem("favoriteItems")) {
            const favoriteItemIds = []
            favoriteItemIds.push(cartItemId);
            localStorage.setItem("favoriteItems", JSON.stringify(favoriteItemIds));
            onAddToFavorites(true);
        } else {
            let currentItems = JSON.parse(localStorage.getItem("favoriteItems"));
            if (currentItems.includes(cartItemId)) {
                currentItems = currentItems.filter(item => item !== cartItemId);
                localStorage.setItem("favoriteItems", JSON.stringify(currentItems));
                onAddToFavorites(false);
            } else {
                currentItems.push(cartItemId);
                localStorage.setItem("favoriteItems", JSON.stringify(currentItems));
                onAddToFavorites(true);
            }
        }
    }

    return (
        <div className="card" ref={currentCard} data-id={props.id}>
            <CardImage className="market_image" altText="placeholder" imgUrl={props.imgUrl}/>
            <SvgElement handleClick={addCardToFavorites}
                        className="svg"
                        svg={<svg width="30px" height="30px" viewBox="0 0 55.867 55.867">
                            <path fill={svgFill} d="M55.818,21.578c-0.118-0.362-0.431-0.626-0.808-0.681L36.92,18.268L28.83,1.876c-0.168-0.342-0.516-0.558-0.896-0.558
	s-0.729,0.216-0.896,0.558l-8.091,16.393l-18.09,2.629c-0.377,0.055-0.689,0.318-0.808,0.681c-0.117,0.361-0.02,0.759,0.253,1.024
	l13.091,12.76l-3.091,18.018c-0.064,0.375,0.09,0.754,0.397,0.978c0.309,0.226,0.718,0.255,1.053,0.076l16.182-8.506l16.18,8.506
	c0.146,0.077,0.307,0.115,0.466,0.115c0.207,0,0.413-0.064,0.588-0.191c0.308-0.224,0.462-0.603,0.397-0.978l-3.09-18.017
	l13.091-12.761C55.838,22.336,55.936,21.939,55.818,21.578z">
                            </path>
                        </svg>}/>
            <div className="card_attributes">
                <h3 className="card_attributes_title">{props.itemTitle}</h3>
                <CardProperty className="card_attributes_price" itemProperty="Price:"
                              propertyDescription={props.price}/>
                <CardProperty className="card_attributes_color" itemProperty="Color:"
                              propertyDescription={props.color}/>
                <CardProperty className="card_attributes_price" itemProperty="Set Number:"
                              propertyDescription={props.setNumber}/>
                {isButtonShown && <Button cardId={cartItemId} modalId="1" className="addToCard_btn"
                                          onAddToCart={addToCart} text="Buy now"/>}
            </div>
        </div>
    );
}

export default Card;