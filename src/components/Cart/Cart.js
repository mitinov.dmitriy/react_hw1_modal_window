import React, {useEffect} from 'react';
import Card from "../Card/Card";
import "./Cart.scss"
import Modal from "../Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {cardsInCart} from "../../store/selectors/selectors.card"
import {setCardsInCart} from "../../store/actions/actions.card"
import {isLoadingFromServer} from "../../store/selectors/selectors.item"
import Loader from "../Loader/Loader";
import CheckoutForm from "../CheckoutForm/CheckoutForm";
import {isCheckoutShown} from "../../store/selectors/selectors.checkOutForm";
import {setToggleOpenCheckoutForm} from "../../store/actions/actions.checkOutForm"

function Cart(props) {
    let {
        items,
        addToFavHandler,
        onRenderHandler,
        isItemRemovedFromCart,
        showRemoveFromCartModal,
        onConfirmRemoveFromCart,
        toggleModal,
        modalToShow
    } = props;

    const cards = useSelector(cardsInCart);
    const isCheckOutFormShown = useSelector(isCheckoutShown)
    const dispatch = useDispatch();

    const isDataLoading = useSelector(isLoadingFromServer);

    useEffect(onRenderHandler)

    useEffect(() => {
        const filteredCards = cards?.filter((card) => {
            return JSON.parse(localStorage.getItem("cartItems")).includes(card.id)

        })
        if (filteredCards) {
            dispatch(setCardsInCart(filteredCards));
        }
    }, [isItemRemovedFromCart])

    useEffect(() => {
        if (localStorage.getItem("cartItems")) {
            const cartItems = items?.filter((element) => {
                return JSON.parse(localStorage.getItem("cartItems")).includes(element.id);
            })
            dispatch(setCardsInCart(cartItems));
        }
    }, [items])

    const toggleCheckOutForm = () => {
        dispatch(setToggleOpenCheckoutForm(!isCheckOutFormShown))
    }

    return (
        <>
            <div className="cartItems">
                {isDataLoading && <Loader/>}
                {cards && cards.map(item => {
                    return <div key={item.set_number} className="cart_wrapper">
                    <span data-cardid={item.id} data-modal="2" className="delete"
                          onClick={showRemoveFromCartModal}/>
                        <Card isButtonShown={false} id={item.id} onAddToFavorites={addToFavHandler}
                              toggleModal={toggleModal} svgFill={localStorage.getItem("favoriteItems")
                        && JSON.parse(localStorage.getItem("favoriteItems")).includes(item.id) ? "#F6BE00" : "darkgray"}
                              itemTitle={item.Title}
                              price={item.Price} color={item.Color}
                              setNumber={item.set_number} imgUrl={item.imageUrl} addToFav={addToFavHandler}
                              addToCart={toggleModal}/>
                    </div>
                })
                }
                {modalToShow &&
                    <Modal onCloseModal={toggleModal}
                           header={modalToShow.modalHeaderText}
                           text={modalToShow.modalBodyText} submitHandler={onConfirmRemoveFromCart}/>}
                {isCheckOutFormShown && <CheckoutForm cardsToCheckout={cards}/>}
            </div>

            <div className="checkout">
                {cards.length > 0 && <button className="checkout_btn" onClick={toggleCheckOutForm}>Buy</button>}
            </div>
        </>

    )
}

export default Cart;