import {useState} from "react";

const useCardId = () => {
    const [cardId, setCardId] = useState(null);
    const changeCardId = (id) => {
        setCardId(id)
    }
    return [cardId, changeCardId];
};

export default useCardId;