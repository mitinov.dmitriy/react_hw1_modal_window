const modalWindowParameters = [
    {
        id: 1,
        modalHeaderText: "Add selected item to cart?",
        modalBodyText: "Please proceed to checkout or add more items"
    },
    {
        id: 2,
        modalHeaderText: "Delete selected item from cart?",
        modalBodyText: "Are you sure you want to delete item?"
    }
]

export default modalWindowParameters;